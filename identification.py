from __future__ import print_function, division
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import os
from PIL import ImageFile, Image
import glob
import io

from modelUtil import *

num_anchor = 100
num_test = 100
pattern1 = 'final_testset'
pattern2 = 'final_anchor'
csv_name = 'result_test'

def main():
    result_csv = io.open(csv_name+'.csv', 'w', encoding='utf-8')
    # Resnet18 trained on raw beautyAI
    model_1 = load_model(my_resnet18, name='resnet18_data5K-2.pt')
    # model_1 = load_model(my_resnet18, name='blend_white5K_best.pt')
    identification(model_1, result_csv)

    
def identification(model, result_csv):
    count = 0
    img_list = glob.glob(os.path.join(pattern1, '*.*'))
    # load anchor
    anchor_tensor, anchor_dict = load_pt()
    print('anchor_tensor', anchor_tensor.shape)
    for img_path in img_list:
        if count>=num_test: break
        count += 1
        # load test set image
        img = load_data(img_path)
        img_tensor = torch.squeeze(model(img))
        # compute dist and nearest neighbor
        dist = torch.norm(anchor_tensor-img_tensor, 2, 1)
        m_y, m_idx = torch.min(dist, 0)
        idx = anchor_dict[m_idx]
        # write to csv
        _, fname = os.path.split(img_path)
        fname = fname.split('.')[0]
        print('Process: ', count, ' / img: ', fname, ' / idx: ', idx, ' / score', m_y)
        result_csv.write(u'{},{},{},{}\n'.format(count, fname, idx, m_y))

    
def load_pt():
    pt_list = glob.glob(os.path.join(pattern2, '*.pt'))

    anchor = []
    anchor_dict = []
    for pt_path in pt_list:
        if len(anchor_dict)>=num_anchor: break
        # anchor_tensor
        tsr = torch.load(pt_path)
        anchor.append(tsr)
        # anchor_dict
        _, fname = os.path.split(pt_path)
        fname = fname.split('.')[0]
        anchor_dict.append(fname)
        
    anchor_tensor = torch.stack(anchor)
    return anchor_tensor, anchor_dict

    
def load_data(path):
    # Normalization
    normalize = transforms.Normalize(
       mean=[0.576, 0.519, 0.465],
       std=[0.216, 0.222, 0.229]
    )
    preprocess = transforms.Compose([
       transforms.Resize(224),
       transforms.CenterCrop(224),
       transforms.ToTensor(),
       normalize
    ])

    # Load data
    img = Image.open(path)
    img_tensor = preprocess(img)
    img_tensor.unsqueeze_(0)
    return img_tensor


def load_model(myModel, name=None):
    # Load the pre-trained model, replace the output layer
    pyModel, _, _ = myModel({})
    # print('pyModel',list(pyModel.children()))
    if name:
        path = os.path.join('output_model', name)
        pyModel.load_state_dict(torch.load(path))
    new_model = nn.Sequential(*list(pyModel.children())[:-1])
    # print('new_model',list(new_model.children()))
    return new_model
    

    

if __name__ == '__main__':
    main()




