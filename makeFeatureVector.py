import os
import glob
import numpy as np
import cv2
from PIL import Image
import torch

from modelUtil import *


source_dir = 'lsdata520K/val'
save_dir = 'final_anchor'
os.makedirs(save_dir, exist_ok=True)

def main():
    model = load_model(my_resnet18, name='resnet18_data5K-2.pt')
    
    img_list = glob.glob(os.path.join(source_dir, '*.*'))
    
    cnt = 0
    for img_path in img_list:
        # print status
        if cnt % 1000==0:
            print('Status: ', cnt)
        cnt += 1    
        # file name
        _, fname = os.path.split(img_path)
        fname = fname.split('.')[0]
        # load data to model
        img = load_data(img_path)
        # print(img_path)
        try:
            img_tensor = torch.squeeze(model(img))
        except RuntimeError:
            continue
        torch.save(img_tensor, os.path.join(save_dir, fname+'.pt'))


def load_data(path):
    # Normalization
    normalize = transforms.Normalize(
       mean=[0.576, 0.519, 0.465],
       std=[0.216, 0.222, 0.229]
    )
    preprocess = transforms.Compose([
       transforms.Resize(224),
       transforms.CenterCrop(224),
       transforms.ToTensor(),
       normalize
    ])

    # Load data
    img = Image.open(path)
    img_tensor = preprocess(img)
    img_tensor.unsqueeze_(0)
    return img_tensor


def load_model(myModel, name=None):
    # Load the pre-trained model, replace the output layer
    pyModel, _, _ = myModel({})
    # print('pyModel',list(pyModel.children()))
    if name:
        path = os.path.join('output_model', name)
        pyModel.load_state_dict(torch.load(path))
    new_model = nn.Sequential(*list(pyModel.children())[:-1])
    # print('new_model',list(new_model.children()))
    return new_model
    
    
    

if __name__ == '__main__':
    main()