from __future__ import print_function, division
import argparse as _argparse
import io
import os
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms

from dataReader import DataReader
from model import Model
from modelUtil import *
from myDataSet import *


# Param: model
DEFAULT_MODEL_NAME = 'Resnet18'
# Param: paths
SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
DEFAULT_DATA_DIR = os.path.join(SCRIPT_PATH, 'tfdata')
OUTPUT_DIR = os.path.join(SCRIPT_PATH, DEFAULT_MODEL_NAME)
# Param: train
DEFAULT_NUM_CLASS = 8
DEFAULT_INPUT_SIZE = 512
DEFAULT_TRAIN_STEPS = 1
DEFAULT_PRINT_STEPS = 10
DEFAULT_EVAL_STEPS = 10
BATCH_SIZE = 8
DEFAULT_USE_GPU = False


def main(data_params, model_params):
    # read data
    dsets = cvs2dataset2(data_params['path_data'])
    # dsets = folder2dataset(data_params['path_data'])
    dset_reader = DataReader(data_params, dsets)
    # print(dset_reader)
    
    # train model
    model = Model(model_params)
    model.build_model(my_siamese5)
    # model.load_model_weights('output_model/resnet18_BlendData5K_L2reg.pt', 0)
    model.train_model(dset_reader)
    model.export_model(option='final')
    model.eval_model(dset_reader, mode='val')
    # model.eval_model(dset_reader, mode='train')

if __name__ == '__main__':
    parser = _argparse.ArgumentParser()
    
    parser.add_argument("-gpu", "--use_gpu", type=bool, help="use gpu or not",
                        default=DEFAULT_USE_GPU)
    parser.add_argument("-nc", "--num_class", type=int, help="# of class",
                        default=DEFAULT_NUM_CLASS)
    parser.add_argument("-iz", "--input_size", type=int, help="Image Size",
                        default=DEFAULT_INPUT_SIZE)
    parser.add_argument("-bz", "--batch_size", type=int, help="Batch Size",
                        default=BATCH_SIZE)
    parser.add_argument("-name", "--model_name", type=str, help="Model Name",
                        default=DEFAULT_MODEL_NAME)
    parser.add_argument("-nts", "--num_train", type=int, help="# of training epoches",
                        default=DEFAULT_TRAIN_STEPS)
    parser.add_argument("-nps", "--num_print", type=int, help="# of print steps",
                        default=DEFAULT_PRINT_STEPS)
    parser.add_argument("-nes", "--num_eval", type=int, help="# of eval steps",
                        default=DEFAULT_EVAL_STEPS)
    parser.add_argument("-data", "--path_data", type=str, help="Path of data folder",
                        default=DEFAULT_DATA_DIR)
    parser.add_argument("-lr", "--learning_rate", type=float, help="Learning rate",
                        default=1e-4)
    parser.add_argument("-reg", "--regularization", type=float, help="L2 regularization strength",
                        default=0)
    args = parser.parse_args()
    
    data_params = {'path_data': args.path_data, 'batch_size': args.batch_size}
    model_params = {'model_name': args.model_name, 'num_class': args.num_class, 
                    'input_size': args.input_size, 'num_eval': args.num_eval, 
                    'num_train': args.num_train, 'num_print': args.num_print,
                    'use_gpu': args.use_gpu, 'lr': args.learning_rate, 
                    'reg': args.regularization}
    main(data_params, model_params)
