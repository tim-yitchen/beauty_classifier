import os
import csv
import torch
from torchvision import datasets
from torch.utils import data
from PIL import Image

from dataReaderUtil import *



def folder2dataset(dir):
    transform_fun = data_transform2('blend_data5K')
    # transform_fun = data_transform1()
    
    dsets = {x: datasets.ImageFolder(os.path.join(dir, x), transform_fun[x])
                            for x in ['train', 'val']}
    return dsets

    
def cvs2dataset(dir):
    dsets = {}
    for mode in ['train', 'val']:
        path = os.path.join(dir, mode+'.csv')
        inputs = []
        labels = []
        with open(path) as f:
            reader = csv.reader(f)
            for row in reader:
                inputs.append(row[0])
                labels.append(int(row[1]))
        dsets[mode] = Dataset(inputs, labels)
    return dsets


def cvs2dataset2(dir):
    '''
    For the model "my_siamese2"
    Input: two images
    Label: integer
    '''
    transform_fun = data_transform2('blend_data5K')
    dsets = {}
    for mode in ['train', 'val']:
        path = os.path.join(dir, mode+'.csv')
        inputs = []
        labels = []
        with open(path) as f:
            reader = csv.reader(f)
            for row in reader:
                inputs.append((row[0], row[1]))
                labels.append(int(row[2]))
        dsets[mode] = Dataset2(inputs, labels, transform_fun[mode])
    return dsets


class Dataset(data.Dataset):
    'Characterizes a dataset for PyTorch'
    def __init__(self, inputs, labels):
        'Initialization'
        self.labels = labels
        self.inputs = inputs
        self.classes = set(labels)

    def __len__(self):
        'Denotes the total number of samples'
        return len(self.inputs)

    def __getitem__(self, index):
        'Generates one sample of data'
        # Load data and get label
        # X = torch.load(self.inputs[index])
        X = torch.load(self.inputs[index])
        y = self.labels[index]
        return X, y
        
        
class Dataset2(data.Dataset):
    '''
    For the model "my_siamese2"
    '''
    'Characterizes a dataset for PyTorch'
    def __init__(self, inputs, labels, transform_fun):
        'Initialization'
        self.labels = labels
        self.inputs = inputs
        self.classes = set(labels)
        self.transform_fun = transform_fun

    def __len__(self):
        'Denotes the total number of samples'
        return len(self.inputs)

    def __getitem__(self, index):
        'Generates one sample of data'
        # Load data and get label
        # X = torch.load(self.inputs[index])
        X1 = self.transform_fun(Image.open(self.inputs[index][0]))
        X2 = self.transform_fun(Image.open(self.inputs[index][1]))
        X = [X1, X2]
        y = self.labels[index]
        return X, y        


def myCollate(batch):
    batch_inputs = torch.stack([b[0] for b in batch], dim=0)
    batch_labels = torch.LongTensor([b[1] for b in batch])
    return batch_inputs, batch_labels


def myCollate2(batch):
    '''
    For the model "my_siamese2"
    Input: two images
    '''
    input1 = torch.stack([b[0][0] for b in batch], dim=0)
    input2 = torch.stack([b[0][1] for b in batch], dim=0)
    batch_inputs = torch.stack([input1, input2])
    batch_labels = torch.LongTensor([b[1] for b in batch])
    return batch_inputs, batch_labels    