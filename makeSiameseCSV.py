import os
import io
import glob
import numpy as np


def label_all_classes(dir, mode, num_data):
    print('----- start doing {} -----'.format(mode))
    labels_csv = io.open(os.path.join(dir, mode+'.csv'), 'w', encoding='utf-8')
    
    image_pattern = os.path.join(dir, mode, '**', '*.*')
    files1 = glob.glob(image_pattern)
    
    for i in range(num_data):
        # get an image
        image_path1 = np.random.choice(files1)
        dir1, fname1 = os.path.split(image_path1)
        id1, rep1 = fname1.split('_')
        
        # positive (label = 1)
        image_path2 = image_path1
        image_pattern2 = os.path.join(dir1, id1 + '_*.*')
        files2 = glob.glob(image_pattern2)
        while image_path2 == image_path1:
            image_path2 = np.random.choice(files2)
        labels_csv.write(u'{},{},{}\n'.format(image_path1, image_path2, 1))
        
        # negative (label = 0)
        id2 = id1
        while id2 == id1:
            image_path2 = np.random.choice(files1)
            dir2, fname2 = os.path.split(image_path2)
            id2, rep2 = fname2.split('_')
        labels_csv.write(u'{},{},{}\n'.format(image_path1, image_path2, 0))

    labels_csv.close()
    print('----- Finish labeling all data -----')


if __name__ == '__main__':
    dir = os.path.join('blend_data_white5K')
    
    mode = 'val'
    num_data = 256
    label_all_classes(dir, mode, num_data)
    
    mode = 'train'
    num_data = 10000
    label_all_classes(dir, mode, num_data)
    
    
    
    
