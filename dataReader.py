import numpy as np
import os

import torch
from torchvision import datasets, models, transforms

from dataReaderUtil import *
from myDataSet import *


class DataReader():
    def __init__(self, data_params, dsets):
        self.data_dir = data_params['path_data']
        self.dsets = dsets
        self.batch_size = data_params['batch_size']
        
        # properties
        self.dset_sizes = {x: len(self.dsets[x]) for x in ['train', 'val']}
        self.dset_classes = self.dsets['train'].classes
        
    def __str__(self):
        print('== Directory of Data Set ==')
        print(self.data_dir)
        print('== Size of Data Set ==')
        print('Train:  ' + str(self.dset_sizes['train']))
        print('Val:    ' + str(self.dset_sizes['val']))
        print('== Classes of Data Set ==')
        print(self.dset_classes)
        return ' '
                    
    def dset_loaders(self):
        dset_loaders = {x: torch.utils.data.DataLoader(self.dsets[x], batch_size=self.batch_size,
                                                shuffle=True, num_workers=1, collate_fn=myCollate2)
                                                for x in ['train', 'val']}
        return dset_loaders

 
# The structure of data should look like this : 
# data_dar
#      |- train 
#            |- dogs
#                 |- dog_image_1
#                 |- dog_image_2
#                        .....

#            |- cats
#                 |- cat_image_1
#                 |- cat_image_1
#                        .....
#            |- humans
#      |- val
#            |- dogs
#            |- cats
#            |- humans


