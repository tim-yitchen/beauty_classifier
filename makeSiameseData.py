import os
import glob
import numpy as np
import cv2
from PIL import Image
import torch

from makeBlendData import blend_an_image
from dataReaderUtil import data_transform2
from model import Model
from modelUtil import *


def load_a_pair(image_path1, image_path2):
    _, name1 = os.path.split(image_path1)
    name1 = name1.split('.')[0]
    _, name2 = os.path.split(image_path2)
    name2 = name2.split('.')[0]
    blend1 = blend_an_image(image_path1, bk_list)
    blend2 = blend_an_image(image_path2, bk_list)
    
    return blend1, blend2, name1, name2

source_dir = 'tfdata5K'
output_dir = 'siamese_data8C5K'
bk_list = glob.glob('bkdata/background_square256/*.*')
preprocess = data_transform2('blend_data5K')['val']    
    
repetition = 5
write_image = False
write_tensor = True

# Make sure thedir exist
for mode in ['train', 'val']:
    for label in ['positive', 'negative']:
        path = os.path.join(output_dir, mode, label)
        if not os.path.exists(path):
            os.makedirs(path)  

# Generate data
for mode in ['val', 'train']:
    image_list = glob.glob('{}/{}/**/*.*'.format(source_dir, mode))
    for image_path1 in image_list:
        print(image_path1)
        for label in ['positive', 'negative']:
            
            if label=='positive':
                image_path2 = image_path1
            if label=='negative':
                while image_path1 == image_path2:
                    image_path2 = np.random.choice(image_list)
                  
            for rep in range(repetition): 
                # get blended images
                blend1, blend2, name1, name2 = load_a_pair(image_path1, image_path2)
                fname = name1+'_'+name2+'_'+str(rep)
                # write images
                if write_image:
                    img = np.concatenate((blend1, blend2), axis=1)
                    cv2.imwrite(os.path.join(output_dir, mode, label, fname+'.jpg'), img)
                # write pytorch tensor
                if write_tensor:
                    torch.backends.cudnn.benchmark = True
                
                    blend1_pil = Image.fromarray(np.uint8(blend1))
                    blend1_pi2 = Image.fromarray(np.uint8(blend2))
                    blendTensor1 = preprocess(blend1_pil)
                    blendTensor2 = preprocess(blend1_pi2)
                    blendTensor1 = torch.reshape(blendTensor1, (1,)+blendTensor1.shape)
                    blendTensor2 = torch.reshape(blendTensor2, (1,)+blendTensor2.shape)
                    blendTensor1 = blendTensor1.to(device=torch.device('cuda'), dtype=torch.float)
                    blendTensor2 = blendTensor2.to(device=torch.device('cuda'), dtype=torch.float)
                    
                    pyModel, _, _ = my_resnet18_dropout({'num_classes':8, 'lr':0, 'reg':0})
                    pyModel = pyModel.cuda()
                    path = 'output_model/resnet18_BlendData5K_L2reg2_dropout_best.pt'
                    pyModel.load_state_dict(torch.load(path))
                    pyModel = nn.Sequential(*list(pyModel.children())[:-1])
                    
                    v1 = torch.squeeze(pyModel(blendTensor1))
                    v2 = torch.squeeze(pyModel(blendTensor2))
                    
                    v_diff = torch.abs(v1-v2)
                    v_diff = v_diff.to(device=torch.device('cpu'), dtype=torch.float)
                    v_diff = torch.tensor(v_diff, requires_grad=True)

                    torch.save(v_diff, os.path.join(output_dir, mode, label, fname+'.pt'))
        
