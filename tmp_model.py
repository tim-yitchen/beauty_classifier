from modelUtil import *
import os
import torch


# pyModel, _, _ = my_resnet18({'num_classes':8})
pyModel, _, _ = my_siamese3({})
# print(pyModel.state_dict().keys())
pyModel.fc3 = myIdentity()
for n, layer in enumerate(list(pyModel.children())):
    print('------')
    print(n)
    print(layer)
    
    
# new_model = nn.Sequential(*list(pyModel.children())[:-1])


# pyModel = torch.nn.DataParallel(pyModel, device_ids=range(torch.cuda.device_count()))
# print(pyModel)


print('===== BEFORE =========')
# plist = [p for p in pyModel.conv1.parameters()]
# print(plist[0][0,0,:,:])
# print(pyModel.state_dict().keys())


print('===== AFTER =========')
# path = os.path.join('output_model', 'resnet18_BlendData5K_L2reg.pt')
# pyModel.load_state_dict(torch.load(path))
# plist = [p for p in pyModel.conv1.parameters()]
# print(plist[0][0,0,:,:])
# print(torch.load(path).keys())




# p = [param for param in pyModel.parameters()]
# print(p)
