from __future__ import print_function, division
import os
import numpy as np
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
from torchvision import datasets, models, transforms


def my_resnet18(params):
    num_classes = params.get("num_classes", 8)
    # Load the pre-trained model, replace the output layer
    pyModel = models.resnet18(pretrained=True)
    num_ftrs = pyModel.fc.in_features
    pyModel.fc = nn.Linear(num_ftrs, num_classes)
    # Define loss and optimizer
    pyCriterion = nn.CrossEntropyLoss()
    pyOptimizer = optim.RMSprop(pyModel.parameters(), lr=1e-5, weight_decay=1e-5)
    return pyModel, pyCriterion, pyOptimizer


def my_resnet18_dropout(params):
    lr = params.get("lr", 3e-5)
    reg = params.get("reg", 1e-3)
    num_classes = params.get("num_classes", 8)
    # Load the pre-trained model
    pyModel = models.resnet18(pretrained=True)
    # Add dropout
    avg_pool = pyModel.avgpool
    pyModel.avgpool = nn.Sequential(
            avg_pool,
            nn.Dropout()
        ) 
    # Replace the output layer
    num_ftrs = pyModel.fc.in_features
    pyModel.fc = nn.Linear(num_ftrs, num_classes)
    # Define loss and optimizer
    pyCriterion = nn.CrossEntropyLoss()
    pyOptimizer = optim.RMSprop(pyModel.parameters(), lr=lr, weight_decay=reg)
    return pyModel, pyCriterion, pyOptimizer
    
    
def my_siamese(params):
    '''
    The data has been pre-processed. The data is the difference of two vectors from a CNN.
    The model is just a linear calssifier.
    '''
    num_input = params.get("input_size", 512)
    num_output = params.get("num_classes", 2)
    lr = params.get("lr", 3e-5)
    reg = params.get("reg", 1e-3)
    
    # Define Model
    pyModel = nn.Sequential(
            nn.Linear(num_input, num_output),
            nn.Sigmoid()
          )
    # Define loss and optimizer
    pyCriterion = nn.CrossEntropyLoss()
    pyOptimizer = optim.Adam(pyModel.parameters(), lr=lr, betas=(0.9, 0.999), weight_decay=reg)
    return pyModel, pyCriterion, pyOptimizer


def my_siamese2(params):
    '''
    The data has been pre-processed. The data is the difference of two vectors from a CNN.
    The model is just a linear calssifier.
    '''
    num_input = params.get("input_size", 512)
    num_output = params.get("num_classes", 2)
    lr = params.get("lr", 3e-5)
    reg = params.get("reg", 1e-3)
    
    # Load Model 
    pyModel, _, _ = my_resnet18({'num_classes':8})
    path = os.path.join('output_model', 'blend_white5K_best.pt')
    pyModel.load_state_dict(torch.load(path))
    pyModel = nn.Sequential(*list(pyModel.children())[:-1])
    for param in pyModel.parameters():
        param.requires_grad = False
    
    # Define Model
    new_model = Net_my_siamese2(pyModel)
    
    # Define loss and optimizer
    pyCriterion = nn.CrossEntropyLoss()
    # pyOptimizer = optim.Adam(new_model.parameters(), lr=lr, betas=(0.9, 0.999), weight_decay=reg)
    model_weights = filter(lambda p: p.requires_grad, new_model.parameters())
    pyOptimizer = optim.Adam(model_weights, lr=lr, betas=(0.9, 0.999), weight_decay=reg)
    
    return new_model, pyCriterion, pyOptimizer    
    
    
def my_siamese3(params):
    '''
    The data has been pre-processed. The data is the difference of two vectors from a CNN.
    The model is just a linear calssifier.
    '''
    num_input = params.get("input_size", 512)
    num_output = params.get("num_classes", 2)
    lr = params.get("lr", 3e-5)
    reg = params.get("reg", 1e-3)
    
    # Load Model 
    pyModel, _, _ = my_resnet18({'num_classes':8})
    path = os.path.join('output_model', 'blend_white5K_best.pt')
    pyModel.load_state_dict(torch.load(path))
    pyModel = nn.Sequential(*list(pyModel.children())[:-1])
    for param in pyModel.parameters():
        param.requires_grad = False
    
    # Define Model
    new_model = Net_my_siamese3(pyModel)
    
    # Define loss and optimizer
    pyCriterion = nn.CrossEntropyLoss()
    # pyOptimizer = optim.Adam(new_model.parameters(), lr=lr, betas=(0.9, 0.999), weight_decay=reg)
    model_weights = filter(lambda p: p.requires_grad, new_model.parameters())
    pyOptimizer = optim.Adam(model_weights, lr=lr, betas=(0.9, 0.999), weight_decay=reg)
    
    return new_model, pyCriterion, pyOptimizer  

def my_siamese4(params):
    '''
    The data has been pre-processed. The data is the difference of two vectors from a CNN.
    The model is just a linear calssifier.
    '''
    num_input = params.get("input_size", 512)
    num_output = params.get("num_classes", 2)
    lr = params.get("lr", 3e-5)
    reg = params.get("reg", 1e-3)
    
    # Load Model 
    pyModel, _, _ = my_resnet18({'num_classes':8})
    path = os.path.join('output_model', 'blend_white5K_best.pt')
    pyModel.load_state_dict(torch.load(path))
    pyModel = nn.Sequential(*list(pyModel.children())[:-1])
    for param in pyModel.parameters():
        param.requires_grad = False
    
    # Define Model
    new_model = Net_my_siamese4(pyModel)
    
    # Define loss and optimizer
    pyCriterion = nn.CrossEntropyLoss()
    # pyOptimizer = optim.Adam(new_model.parameters(), lr=lr, betas=(0.9, 0.999), weight_decay=reg)
    model_weights = filter(lambda p: p.requires_grad, new_model.parameters())
    pyOptimizer = optim.Adam(model_weights, lr=lr, betas=(0.9, 0.999), weight_decay=reg)
    
    return new_model, pyCriterion, pyOptimizer      
    
def my_siamese5(params):
    '''
    The data has been pre-processed. The data is the difference of two vectors from a CNN.
    The model is just a linear calssifier.
    '''
    num_input = params.get("input_size", 512)
    num_output = params.get("num_classes", 2)
    lr = params.get("lr", 3e-5)
    reg = params.get("reg", 1e-3)
    
    # Load Model 
    pyModel, _, _ = my_resnet18({'num_classes':8})
    path = os.path.join('output_model', 'blend_white5K_best.pt')
    pyModel.load_state_dict(torch.load(path))
    pyModel = nn.Sequential(*list(pyModel.children())[:-1])
    for param in pyModel.parameters():
        param.requires_grad = False
    
    # Define Model
    new_model = Net_my_siamese5(pyModel)
    
    # Define loss and optimizer
    pyCriterion = nn.CrossEntropyLoss()
    # pyOptimizer = optim.Adam(new_model.parameters(), lr=lr, betas=(0.9, 0.999), weight_decay=reg)
    model_weights = filter(lambda p: p.requires_grad, new_model.parameters())
    pyOptimizer = optim.Adam(model_weights, lr=lr, betas=(0.9, 0.999), weight_decay=reg)
    
    return new_model, pyCriterion, pyOptimizer        
    
    
class Net_my_siamese2(nn.Module):
    '''
    For the model "my_siamese2"
    '''
    def __init__(self, extractor):
        super(Net_my_siamese2, self).__init__()
        self.extractor = extractor
        self.fc1 = nn.Linear(512, 512)
        self.fc2 = nn.Linear(512, 2)

    def forward(self, x):
        v1 = self.extractor(x[0])
        v2 = self.extractor(x[1])
        y = torch.abs(v1 - v2)
        y = torch.squeeze(y)
        y = F.relu(self.fc1(y))
        y = self.fc2(y)
        return y

class Net_my_siamese3(nn.Module):
    '''
    For the model "my_siamese3"
    '''
    def __init__(self, extractor):
        super(Net_my_siamese3, self).__init__()
        self.extractorA = nn.Sequential(*list(extractor.children())[0:4])
        self.extractorB = nn.Sequential(*list(extractor.children())[5:])
        self.fc1 = nn.Linear(256, 512)
        self.pool1 = nn.MaxPool2d(28,stride=28)
        self.fc2 = nn.Linear(512, 512)
        self.fc3 = nn.Linear(1024, 2)

    def forward(self, x):
        v1 = self.extractorA(x[0])
        v2 = self.extractorA(x[1])
        yA = torch.abs(v1 - v2)
        yA = self.pool1(yA)
        yA = torch.reshape(yA,(-1,256))
        yA = F.relu(self.fc1(yA))
        
        v1 = self.extractorB(v1)
        v2 = self.extractorB(v2)
        yB = torch.abs(v1 - v2)
        yB = torch.reshape(yB,(-1,512))
        yB = F.relu(self.fc2(yB))
        
        y = torch.cat([yA, yB], dim=1)
        y = self.fc3(y)
        return y        
        
class Net_my_siamese4(nn.Module):
    '''
    For the model "my_siamese4"
    '''
    def __init__(self, extractor):
        super(Net_my_siamese4, self).__init__()
        self.extractor = extractor
        self.fc1 = nn.Linear(512, 2)

    def forward(self, x):
        v1 = self.extractor(x[0])
        v2 = self.extractor(x[1])
        y = torch.abs(v1 - v2)
        y = torch.squeeze(y)
        y = self.fc1(y)
        return y

class Net_my_siamese5(nn.Module):
    '''
    For the model "my_siamese3"
    '''
    def __init__(self, extractor):
        super(Net_my_siamese5, self).__init__()
        self.extractorA = nn.Sequential(*list(extractor.children())[0:4])
        self.extractorB = nn.Sequential(*list(extractor.children())[5:])
        self.pool1 = nn.MaxPool2d(28,stride=28)
        self.fc1 = nn.Linear(256+512, 2)

    def forward(self, x):
        v1 = self.extractorA(x[0])
        v2 = self.extractorA(x[1])
        yA = torch.abs(v1 - v2)
        yA = self.pool1(yA)
        yA = torch.reshape(yA,(-1,256))
        
        v1 = self.extractorB(v1)
        v2 = self.extractorB(v2)
        yB = torch.abs(v1 - v2)
        yB = torch.reshape(yB,(-1,512))
        
        y = torch.cat([yA, yB], dim=1)
        y = self.fc1(y)
        return y           
        
def build_model_alexnet(num_classes):
    # Load the pre-trained model, replace the output layer
    pyModel = models.alexnet(pretrained=True)
    num_ftrs = pyModel.classifier[6].in_features
    pyModel.classifier[6] = nn.Linear(num_ftrs, num_classes)
    # Define loss and optimizer
    pyCriterion = nn.CrossEntropyLoss()
    pyOptimizer = optim.RMSprop(pyModel.parameters(), lr=0.0001)
    return pyModel, pyCriterion, pyOptimizer
    
    
class myIdentity(nn.Module):
    def forward(self, inputs):
        return inputs