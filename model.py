from __future__ import print_function, division
import numpy as np
import os
import time
from datetime import datetime
import copy

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torchvision
from torchvision import datasets, models, transforms
from tensorboardX import SummaryWriter


class Model():
    def __init__(self, model_params): 
        # Param: model
        self.params = model_params
        self.model_name = model_params['model_name']
        # Param: save paths
        SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
        self.save_dir = os.path.join(SCRIPT_PATH, 'output_model')
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)
        # Param: train
        self.counter = 0
        self.num_class = model_params['num_class']
        self.input_size = model_params['input_size']
        self.num_train_epoch = model_params['num_train']
        self.num_print_steps = model_params['num_print']
        self.num_eval_steps  = model_params['num_eval']
        # pytorch object
        self.model = None
        self.criterion = None
        self.optimizer = None
        self.best_model = None
        self.best_valacc = 0.0
        # GPU
        self.use_gpu = model_params['use_gpu']
        if self.use_gpu and torch.cuda.is_available():
            self.device = torch.device('cuda')
            torch.backends.cudnn.benchmark = True
        else:
            self.device = torch.device('cpu')
    
    
    def build_model(self, build_fnc):
        ''' The "build_fnc" should retrun "model", "criterion" and "optimizer". 
                                   accept "num_classes". '''
        self.model, self.criterion, self.optimizer = build_fnc(self.params)
        if self.use_gpu and torch.cuda.is_available(): 
            self.model = self.model.cuda()
            # Use "torch.nn.DataParallel" only when there are multiple GPUs:
            # self.model = torch.nn.DataParallel(self.model, device_ids=range(torch.cuda.device_count()))
     
     
    def load_model_weights(self, path, counter=0):
        # path = os.path.join('output model', 'best_model.pt')
        self.model.load_state_dict(torch.load(path))
        self.counter = counter


    def train_model(self, dset_reader):        
        model = self.model
        criterion = self.criterion
        optimizer = self.optimizer
        
        current_time = datetime.now().strftime('%b%d_%H-%M-%S')
        summary_dir  = 'runs_'+dset_reader.data_dir
        summary_name = 'lr='+str(np.log10(self.params['lr']))+'_reg'+str(np.log10(self.params['reg']))+'_'+current_time
        tb_writer = SummaryWriter(log_dir = os.path.join(summary_dir, summary_name))
        since = time.time()
        counter = self.counter
        for epoch in range(self.num_train_epoch):
            print('=' * 10)
            print('Epoch {}/{}'.format(epoch, self.num_train_epoch - 1))
            print('=' * 10)

            model.train()  # Set model to training mode
            running_loss = 0.0
            running_corrects = 0
            # Iterate over data.
            dset_loader = dset_reader.dset_loaders()['train']
            for inputs, labels in dset_loader:
                # inputs, labels = data
                inputs = inputs.to(device=self.device, dtype=torch.float)
                labels = labels.to(device=self.device, dtype=torch.long)

                # Set gradient to zero to delete history of computations in previous epoch. Track operations so that differentiation can be done automatically.
                optimizer.zero_grad()
                outputs = model(inputs)
                _, preds = torch.max(outputs.data, 1)
                loss = criterion(outputs, labels)

                # backward + optimize only if in training phase
                loss.backward()
                optimizer.step()
                running_loss += loss.data.item()
                running_corrects += torch.sum(preds == labels.data).item()

                # Print Progress
                if counter % self.num_print_steps ==0:
                    # print to screen 
                    batch_loss = loss.data.item()
                    batch_acc = torch.mean((preds == labels.data).type(torch.FloatTensor)).item()
                    print(datetime.now().strftime('%H:%M') + " iteration: %s" % counter)
                    print('{} Loss: {:.3f} Acc: {:.3f}'.format('mini-batch', batch_loss, batch_acc))
                    # print to tensorboard
                    tb_writer.add_scalar('data/train_batch_loss', batch_loss, counter)
                    tb_writer.add_scalar('data/train_batch_acc', batch_acc, counter)
                if counter % self.num_eval_steps  ==0:
                    # print to screen
                    val_loss, val_acc = self.eval_model(dset_reader, mode='val')
                    # print to tensorboard
                    tb_writer.add_scalar('data/val_loss', val_loss, counter)
                    tb_writer.add_scalar('data/val_acc', val_acc, counter)
                    # store the best model
                    if val_acc > self.best_valacc:
                        self.best_model = copy.deepcopy(model)
                
                # counter    
                counter+=1
            
            # Calculate epoch loss
            epoch_loss = running_loss / dset_reader.dset_sizes['train']
            epoch_acc = running_corrects / dset_reader.dset_sizes['train']
            print(' ')
            print('{} Loss: {:.4f} Acc: {:.4f}'.format('train epoch', epoch_loss, epoch_acc))
            print(' ')
            # print to tensorboard
            tb_writer.add_scalar('data/train_epoch_loss', epoch_loss, counter)
            tb_writer.add_scalar('data/train_epoch_acc', epoch_acc, counter)
            time_elapsed = time.time() - since
            time_message = 'Time time_elapsed: {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60)
            tb_writer.add_text('Text', time_message, counter)
            # save best model
            self.export_model(option='best')
            
        # Summary of training
        time_elapsed = time.time() - since
        print('Training complete in {:.0f}m {:.0f}s'.format(
                time_elapsed // 60, time_elapsed % 60))
        # save to tensorboard
        if not os.path.exists('tb_log'):
            os.makedirs('tb_log')
        tb_writer.export_scalars_to_json(os.path.join('tb_log', self.model_name+'.json'))
        tb_writer.close()

        
    def export_model(self, option='both'):
        # save "final" model to a pt file
        if option=='final' or option=='both':
            path = os.path.join(self.save_dir, self.model_name + '_final.pt')
        # save "best" model to a pt file
        if option=='best' or option=='both':
            path = os.path.join(self.save_dir, self.model_name + '_best.pt')
        torch.save(self.best_model.state_dict(), path)

        
    def eval_model(self,dset_reader, mode='val'):
        model = self.model
        criterion = self.criterion
        
        # Initialize
        model.eval() # Set model to eval mode
        running_loss = 0.0
        running_corrects = 0
        # Iterate over data.
        dset_loader = dset_reader.dset_loaders()[mode]
        for data in dset_loader:
            inputs, labels = data
            inputs = inputs.to(device=self.device, dtype=torch.float)
            labels = labels.to(device=self.device, dtype=torch.long)

            # make inference
            outputs = model(inputs)
            _, preds = torch.max(outputs.data, 1)
            loss = criterion(outputs, labels)
            running_loss += loss.data.item()
            running_corrects += torch.sum(preds == labels.data).item()
        
        # Summary of Eval
        epoch_loss = running_loss / dset_reader.dset_sizes[mode]
        epoch_acc = running_corrects / dset_reader.dset_sizes[mode]
        print('-' * 5)
        print('{} Loss: {:.4f} Acc: {:.4f}'.format(mode, epoch_loss, epoch_acc))
        print('-' * 5)
        return epoch_loss, epoch_acc
