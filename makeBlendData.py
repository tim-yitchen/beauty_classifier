import cv2
import numpy as np
import glob
import os


__all__ = ['display', 'get_edges', 'get_contours', 'get_masks', 'alpha_blend']


INTERVAL = 1000 # ms

# Parameters 
ALPHA = 1
BLUR = 21 # odd
# CANNY_THRS = [(1,5), (50, 100), (10, 300), (10, 500)]
CANNY_THRS = [(5,30), (10,50), (50, 100), (50, 300), (100, 500), (300, 600), (500, 900)]
MASK_DILATE_ITER = 30
MASK_ERODE_ITER = 30



def display(image, name='', interval=INTERVAL):

    image = image.astype('uint8')
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name,(300,300))
    cv2.imshow(name, image)
    cv2.waitKey(interval)
    cv2.destroyAllWindows()
    

def get_edges(image, canny_min, canny_max):

    edges = cv2.Canny(image, canny_min, canny_max)
    edges = cv2.dilate(edges, None)
    edges = cv2.erode(edges, None)
    return edges

def get_contours(edges):

    contours = cv2.findContours(edges, mode=cv2.RETR_LIST, 
        method=cv2.CHAIN_APPROX_SIMPLE)[1]

    contours = sorted(contours, key=cv2.contourArea, reverse=True)
    return contours

def get_masks(edges, dilate_num=MASK_DILATE_ITER, erode_num=MASK_ERODE_ITER, 
    blur=BLUR, order=0, alpha=ALPHA):

    contours = get_contours(edges)

    try:
        max_contour = contours[order]
    except IndexError:
        print('No contours are found')

    mask = np.zeros(edges.shape)
    cv2.fillConvexPoly(mask, max_contour, (255*alpha))

    mask = cv2.dilate(mask, None, iterations=MASK_DILATE_ITER)
    mask = cv2.erode(mask, None, iterations=MASK_ERODE_ITER)
    mask = cv2.GaussianBlur(mask, (BLUR, BLUR), 0)
    
    mask_3 = np.dstack([mask]*3)
    mask_3  = mask_3.astype('float32') / 255.0

    return mask_3

def alpha_blend(mask_3, tar_image, back_image):

    tar_image = tar_image.astype('float32')
    back_image = back_image.astype('float32')
    # print(tar_image.shape)
    # print(back_image.shape)
    
    output_image = mask_3 * tar_image + (1.0 - mask_3) * back_image
    output_image = output_image
    return output_image

def get_best_mask(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    candidate = []
    candidate_percent = []
    candidate_box = []
    for thr in CANNY_THRS:
        edges = get_edges(gray, *thr)
        for order in range(1):
            try:
                max_contour = get_contours(edges)[order]
            except IndexError:
                break
            else:
                max_area = cv2.contourArea(max_contour)
                total_area = edges.shape[0] * edges.shape[1]
                percentage = 100 * max_area / total_area

                # print('area : %.1f %%' % percentage)
                
                left =  min(max_contour[:,0,0])
                right = max(max_contour[:,0,0])
                top = min(max_contour[:,0,1])
                bottom = max(max_contour[:,0,1])
                bounding_box = (left, right, top, bottom)
                
                mask_3 = get_masks(edges, order=order)
                
                candidate.append(mask_3)
                candidate_percent.append(percentage)
                candidate_box.append(bounding_box)
    
    diff_percent = np.diff(candidate_percent)
    idx = np.argmax(np.abs(diff_percent))
    mask3_best = candidate[idx]
    bounding_box = candidate_box[idx]
    # print('choice: ', idx)
    
    return mask3_best, bounding_box

def random_rotate(image, img_size):
    rand_angle = np.random.uniform(low=-90, high=90)
    M = cv2.getRotationMatrix2D((img_size/2,img_size/2),rand_angle,1)
    image = cv2.warpAffine(image,M,(img_size,img_size),borderValue=(255, 255, 255))
    return image
    
def random_blend(mask_3, bounding_box, tar_image, back_image):
    # load, crop, resize tar_image
    x1, x2, y1, y2 = bounding_box
    tar_image = tar_image.astype('float32')
    tar_image = tar_image[y1:y2, x1:x2]
    mask_3 = mask_3[y1:y2, x1:x2]
    rand_size = np.random.uniform(low=0.4, high=0.8)
    tar_image = cv2.resize(tar_image, (0,0), fx=rand_size, fy=rand_size)
    mask_3 = cv2.resize(mask_3, (0,0), fx=rand_size, fy=rand_size)
    # load back_image 
    back_image = back_image.astype('float32')
    # decide where to put tar_image
    rand_pos  = np.random.uniform()
    cx = int(rand_pos*(back_image.shape[1]-tar_image.shape[1]))
    cy = int(rand_pos*(back_image.shape[0]-tar_image.shape[0]))
    # overlay tar_image on back_image
    output_image = back_image
    x1, x2, y1, y2 = cx, cx+tar_image.shape[1], cy, cy+tar_image.shape[0]
    output_image[y1:y2, x1:x2] = mask_3 * tar_image + (1.0 - mask_3) * back_image[y1:y2, x1:x2]
    
    # print('size', rand_size, '    /    pos', rand_pos)
    return output_image  

    
def blend_an_image(image_path, bk_list):
    # get image
    image = cv2.imread(image_path)
    # if image is None: continue # exception handle
    image = cv2.resize(image , (img_size, img_size), interpolation=cv2.INTER_CUBIC)
    
    # get masked image
    image = random_rotate(image, img_size)
    # display(image, "image")
    mask, bounding_box = get_best_mask(image) # exception handle (ValueError)
          
    x1, x2, y1, y2 = bounding_box
    # if x1==x2 or y1==y2: continue # exception handle

    # get background
    background_path = bk_list[np.random.randint(0, high=len(bk_list))]
    background = cv2.imread(background_path)

    # blended_image
    blended_image = random_blend(mask, bounding_box, image, background)
    # display(blended_image, "blended_image")
    
    return blended_image
    

img_size = 256
    
def main():    
    background_pattern = 'bkdata/background_square256/*.*'
    image_pattern = 'lsdata100/**/*.*'
    save_pattern = 'blend_data'
    repetition = 3
    
    bk_list = glob.glob(background_pattern)
    
    for image_path in glob.iglob(image_pattern, recursive=False):
    # for i in range(1):
        # image_path = os.path.join('tfdata5K','train','hair','665.JPEG')
        print(image_path)
        
        for rep in range(repetition):
            blended_image = blend_an_image(image_path, bk_list)
            
            # save image
            old_path = image_path.split(os.sep)
            old_path[-1] = old_path[-1].split('.')[0] + '_' + str(rep) + '.jpg'
            save_path = os.path.join(save_pattern, *old_path[1:])
            save_dir, _ = os.path.split(save_path)
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)
            
            cv2.imwrite(save_path, blended_image)


if __name__ == '__main__':
    main()
