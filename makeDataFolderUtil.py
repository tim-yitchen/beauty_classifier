def get_classes_list(option=1):
    if option==0:
        classes_list = ['lip','hair','eye','pencil','compact','mask','masque','soap','gel',
                        'spray','lash','palette','powder','kit','sunscreen','oil','shampoo',
                        'butter','concealer','glow','cream','nail','polish','lotion','body',
                        'highlight','deodorant','brush','spa','perfume','massager','toner',
                        'liner','blush','condition','foundation','mascara','scrub',
                        'rollerball','clip','concentrate','razor','scrunch','bikini',' uv','sun',
                        ' spf','wax','hydrafresh','scraper','gift',' set','puff','towel','stick',
                        'facial','hydrator','cleanser','?','/',]
    if option==1:
        classes_list = ['lip', 'hair', 'spray', 'nail', 'mask', 'liner', 'kit', 'palette']
        
        
    if option==2:
        # For siamese network
        classes_list = ['negative', 'positive']
    
    
    classes_dict = {}
    for i in range(len(classes_list)):
        c = classes_list[i]
        classes_dict[c] = i
        
    return classes_list, classes_dict