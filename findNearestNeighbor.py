from __future__ import print_function, division
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import os
from PIL import ImageFile, Image
import glob

from modelUtil import *

# pattern1 = 'test_data100/{}.jpg'.format(i)
# pattern2 = 'lsdata100/val/{}.JPEG'.format(i)
pattern1 = 'test_siamese_white/val'
pattern2 = 'test_siamese_white/test'

def main():
    
    # Resnet18 trained on ImageNet
    # model_0 = load_model(my_resnet18)
    # accu0 = find_accu(model_0)
    # print(accu0)

    # Resnet18 trained on raw beautyAI
    model_1 = load_model(my_resnet18, name='resnet18_data5K-2.pt')
    # model_1 = load_model(my_resnet18, name='blend_white5K_best.pt')
    accu1 = find_accu(model_1)
    print(accu1)
    
    # Resnet18 trained on blended beautyAI 
    # model_2 = load_model(my_resnet18_dropout, name='resnet18_BlendData5K_L2reg.pt')
    # accu2 = find_accu(model_2)
    # print(accu2)
    
    # Resnet18 trained on blended beautyAI + Siamese at last layer (2x affine)
    # model_3 = load_model2(my_siamese2, name='siamese_blend_white5K_best.pt')
    # accu3 = find_accu2(model_3)
    # print(accu3)
    
    # Resnet18 trained on blended beautyAI + Siamese at 1st and last layer (2x affine each)
    # model_4 = load_model2(my_siamese3, name='siamese3_blend_white5K_best.pt')
    # accu4 = find_accu2(model_4)
    # print(accu4)
    
    # Resnet18 trained on blended beautyAI + Siamese at last layer (1x affine)
    # model_5 = load_model2(my_siamese4, name='siamese4_blend_white5K_best.pt')
    # accu5 = find_accu2(model_5)
    # print(accu5)
    
    # Resnet18 trained on blended beautyAI + Siamese at 1st and last layer (1x affine each)
    # model_6 = load_model2(my_siamese5, name='siamese5_blend_white5K_best.pt')
    # accu6 = find_accu2(model_6)
    # print(accu6)
    


def find_accu(model):
    test_set = []
    traget_set = []
    for i in range(99):
        file_path = os.path.join(pattern1,'{}.jpg'.format(i))
        img = load_data(file_path)
        test_set.append(torch.squeeze(model(img)))
        
        file_path = os.path.join(pattern2,'{}.jpg'.format(i))
        img = load_data(file_path)
        traget_set.append(torch.squeeze(model(img)))

    traget_set = torch.stack(traget_set) 
    print('traget_set', traget_set.shape)
    print('test_set', test_set[0].shape)
    
    acc_num = 0
    for i in range(99):
        dist = torch.norm(traget_set-test_set[i], 2, 1)
        ym, idx = torch.min(dist, 0)
        if idx==i:
            acc_num += 1
        print('Process: ', i, ' / idx: ', idx, ' / score', ym)
    return acc_num

    
def find_accu2(model):
    '''
    Siamese Network
    '''
    acc_num = 0
    for i in range(99):
        dist = []
        for j in range(99):
            file_path = os.path.join(pattern1,'{}.jpg'.format(i))
            img1 = load_data(file_path)
            file_path = os.path.join(pattern2,'{}.jpg'.format(j))
            img2 = load_data(file_path)
            y = torch.squeeze(model([img1, img2]))
            y = y[1] - y[0]
            dist.append(y)

        dist = torch.stack(dist) 
        ym, idx = torch.max(dist, 0)
        # print('test_set', dist.shape)
        if idx==i:
            acc_num += 1
        print(i, 'idx: ', idx, 'score', ym)
        
    return acc_num
    
    
def load_data(path):
    # Normalization
    normalize = transforms.Normalize(
       mean=[0.576, 0.519, 0.465],
       std=[0.216, 0.222, 0.229]
    )
    preprocess = transforms.Compose([
       transforms.Resize(224),
       transforms.CenterCrop(224),
       transforms.ToTensor(),
       normalize
    ])

    # Load data
    img = Image.open(path)
    img_tensor = preprocess(img)
    img_tensor.unsqueeze_(0)
    return img_tensor


def load_model(myModel, name=None):
    # Load the pre-trained model, replace the output layer
    pyModel, _, _ = myModel({})
    # print('pyModel',list(pyModel.children()))
    if name:
        path = os.path.join('output_model', name)
        pyModel.load_state_dict(torch.load(path))
    new_model = nn.Sequential(*list(pyModel.children())[:-1])
    # print('new_model',list(new_model.children()))
    return new_model
    
    
def load_model2(myModel, name=None):
    '''
    For Siamese Network
    '''
    # Load the pre-trained model, replace the output layer
    pyModel, _, _ = myModel({})
    # print('pyModel',list(pyModel.children()))
    if name:
        path = os.path.join('output_model', name)
        pyModel.load_state_dict(torch.load(path))
    # print('new_model',list(new_model.children()))
    return pyModel
    

if __name__ == '__main__':
    main()




