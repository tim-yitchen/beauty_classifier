import os
import io
import glob

from makeDataFolderUtil import get_classes_list



def label_a_class(labels_csv, class_name, path, num_data):
    files = glob.glob(os.path.join(path, class_name, '*.pt'))
    filesList = [files[i] for i in range(num_data)]
    class_label = classes_dict[class_name]
    for file_path in filesList:
        labels_csv.write(u'{},{}\n'.format(file_path, class_label))
    print('----- Finish labeling: ' + class_name + ' -----')

def label_all_classes(path, num_data, fname):
    labels_csv = io.open(os.path.join(path, fname+'.csv'), 'w', encoding='utf-8')
    for class_name in classes_list:
        label_a_class(labels_csv, class_name, os.path.join(path,fname), num_data)
    labels_csv.close()
    print('----- Finish labeling all data -----')


if __name__ == '__main__':
    path_data = os.path.join('siamese_data8C5K')
    classes_list, classes_dict = get_classes_list(option=2)
    
    fname = 'val'
    num_data = 256
    label_all_classes(path_data, num_data, fname)
    
    fname = 'train'
    num_data = 4096
    label_all_classes(path_data, num_data, fname)
    
    
    
    
